# -*- coding: utf-8 -*-
from odoo import http, _
from odoo.http import request
from openerp.addons.website.controllers.main import Website


class Website(Website):
    @http.route(auth='public')
    def index(self, data={},**kw):
        super(Website, self).index(**kw)
        categories_list = request.env['product.public.category'].sudo().search([('parent_id','=',False)])
        categories_list_list = []
        categories_dict = {}
        for item in categories_list:
            categories_dict.update({'name':item.name,
                                    'id':item.id,
                                    'category_item':item
                                    })
            categories_list_list.append(categories_dict)
            categories_dict = {}
        data.update({'categories':categories_list_list})
        return http.request.render('alisashop.homepage_alisa', data)
    
    
    @http.route(['/shop/categories/<string:gender>'], type='http', auth="public", website=True)
    def select_category(self, gender, **kw):
        id_of_parent = request.env['product.public.category'].sudo().search([('name','=',gender)])
        categories_list = request.env['product.public.category'].sudo().search([('parent_id','=',id_of_parent.id)])
        data = {}
        categories_list_list = []
        categories_dict = {}
        for item in categories_list:
            categories_dict.update({'name':item.name,
                                    'id':item.id,
                                    'category_item':item
                                    })
            categories_list_list.append(categories_dict)
            categories_dict = {}
        data.update({'categories':categories_list_list})
        data.update({'parent_name':id_of_parent.name})
        return request.render("alisashop.categories_page",data)
    
    