{
    'name': "AlisaShop",
    'version': '1.0',
    'depends': ['base',
                'sale',
],
    'author': "Sergey Stepanets",
    'category': 'Application',
    'description': """
    Customize for asile shop 
    """,
    'data': [
        'views/homepage.xml',
        'views/assets.xml',
        'views/categories.xml',
    ],
}